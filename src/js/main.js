/* eslint-env jquery */

(function() {
  'use strict';

  var EventEmitter = require('events'),
      Event = new EventEmitter(),
      Model = require('./model')(Event),
      View = require('./view')(Event);

  $(document).ready(function() {
    Model.init();
    View.init();
  });
}());
