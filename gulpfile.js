var gulp = require('gulp'),
  browserSync = require('browser-sync'),
  watchify = require('watchify'),
  browserify = require('browserify'),
  babelify = require('babelify'),
  source = require('vinyl-source-stream'),
  buffer = require('vinyl-buffer'),
  gutil = require('gulp-util'),
  sourcemaps = require('gulp-sourcemaps'),
  uglify = require('gulp-uglify'),
  myth = require('gulp-myth'),
  handlebars = require('gulp-handlebars'),
  wrap = require('gulp-wrap'),
  declare = require('gulp-declare'),
  concat = require('gulp-concat');

//*****************************************************************************
// Browserify
//*****************************************************************************

var customOpts = {
  entries: ['./src/js/main.js'],
  debug: true,
  transform: [['babelify']]
};
var opts = Object.assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts));
b.on('update', bundle);
b.on('log', gutil.log);

//*****************************************************************************
// Functions
//*****************************************************************************

function bundle() {
  return b.bundle()
  .on('error', gutil.log.bind(gutil, 'Browserify Error'))
  .pipe(source('main.js'))
  .pipe(buffer())
  .pipe(sourcemaps.init({loadMaps: true}))
  .pipe(uglify())
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest('app/js/'))
  .pipe(browserSync.reload({stream: true}));
}

function css() {
  return gulp.src('src/css/**/*.css')
  .pipe(myth({sourcemap: true, compress: true}))
  .pipe(gulp.dest('app/css'))
  .pipe(browserSync.reload({stream: true}));
}

function templates() {
  gulp.src('src/templates/*.hbs')
  .pipe(handlebars({
    handlebars: require('handlebars')
  }))
  .pipe(wrap('Handlebars.template(<%= contents %>)'))
  .pipe(declare({
    namespace: 'Quiz.templates',
    noRedeclare: true // Avoid duplicate declarations
  }))
  .pipe(concat('templates.js'))
  .pipe(gulp.dest('app/js/'))
  .pipe(browserSync.reload({stream: true}));
}

function serve() {
  browserSync.init({
    server: {
      baseDir: 'app/'
    }
  });

  gulp.watch('src/js/*.js', ['js']);
  gulp.watch('src/css/**/*.css', ['css']);
  gulp.watch('src/templates/*.hbs', ['templates']);
  gulp.watch('app/*.html', browserSync.reload);
}

//*****************************************************************************
// Tasks
//*****************************************************************************

gulp.task('css', css);
gulp.task('js', bundle);
gulp.task('templates', templates);
gulp.task('serve', ['css', 'js', 'templates'], serve);
