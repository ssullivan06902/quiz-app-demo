# Quiz App Demo

This is an example solution to a challenge given to interns at [Big Idea
Interactive Media](http://bigideainteractive.com/).

## Dependencies

[Gulp](http://gulpjs.com/) is used for build management. It should be installed
globally prior to running the server.

`$ npm install --global gulp` (may require root permissions)

After installing gulp, fetch the other project dependencies. From the project
root, execute the following command:

`$ npm install`

## Building & Running

All build tasks are run as dependencies of the `serve` task and will
auto-rebuild on change as long as the server is running. The gulp `serve` task
also enables live browser-reload. Start the build process and the server with
the following command:

`$ gulp serve`
