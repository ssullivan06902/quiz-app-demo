# Ideas for Improvement

If you wanted, you could use this demo as a starting point for something much
more interesting. The obvious direction would be to generalize the app into a 
tool for creating quizzes. You could offer it to the world as a free tool, and
host it as a paid service for those who aren't savvy enough to serve their own
copy.

## Desirable Features

* Select between question types when creating quizzes (multiple choice, select
  all that apply, true/false, matching, etc.)
* Select between automatically/manually graded quizzes. Manually-graded quizzes 
  could have question types that wouldn't work in automatically-graded quizzes
  (ex. short answer, essay).
* Variably weighted questions
* Adjustable grading scales
* The same toolset could be used to create forms/polls
* User-accounts (i.e., student roster) or anonymous quizzes
* Mailing lists for users to send quizzes/results to
* Embedded quizzes
* Themes/Colorschemes

## Necessary Enhancements

* Test suites!